//
//  AchievementsTableViewController.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 10/14/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

class AchievementsTableViewController: UITableViewController {

    //MARK: Data structures
    
    struct AchievementWithImage {
        let name: String
        let kind: String?
        let description: String
        let image: UIImage?
        let multiplier: Int
        
        init(achievement: StudentModel.Achievement, multiplier: Int) {
            self.name = (achievement.name ?? "") + (multiplier > 1 ? " x" + String(multiplier) : "")
            self.kind = achievement.kind
            self.multiplier = multiplier
            self.description = achievement.description ?? ""
            self.image = achievement.image != nil ? GetCachedImage(key: achievement.image!) : nil
        }
    }
    
    //MARK: Properties
    
    var achievements: [(String, [AchievementWithImage])] = []
    
    //MARK: Methods
    
    func mapAchievementPerName(validAchiements: [StudentModel.Achievement]) -> [String:AchievementWithImage] {
        var singleNameAchievements: [String:AchievementWithImage] = [:]
        for achievement in validAchiements {
            let alreadyAdded = singleNameAchievements[achievement.name!]
            let multiplier = alreadyAdded != nil ? alreadyAdded!.multiplier + 1 : 1
            singleNameAchievements[achievement.name!] = AchievementWithImage.init(achievement: achievement, multiplier: multiplier)
        }
        return singleNameAchievements
    }
    
    func mapAchievementPerKind(achievementPerName: [String:AchievementWithImage]) -> [String:[AchievementWithImage]] {
        var singleKindAchievements: [String:[AchievementWithImage]] = [:]
        for pair in achievementPerName {
            if singleKindAchievements[pair.value.kind ?? "other"] == nil {
                singleKindAchievements[pair.value.kind ?? "other"] = []
            }
            singleKindAchievements[pair.value.kind ?? "other"]?.append(pair.value)
        }
        return singleKindAchievements
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard achievements.isEmpty else {
            return
        }

        let validAchiements = GetCurrentStudentData().achievements.filter({ $0.visible ?? true && $0.name != nil })
        let achievementPerKind = mapAchievementPerKind(achievementPerName: mapAchievementPerName(validAchiements: validAchiements))
        achievements = achievementPerKind.compactMap { ($0, $1) }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.title = "Achievements"
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return achievements.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return achievements[section].1.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return achievements[section].0.capitalized
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AchievementTableViewCell", for: indexPath) as? AchievementTableViewCell  else {
            fatalError("The dequeued cell is not an instance of AchievementTableViewCell.")
        }
        
        let achievement = achievements[indexPath.section].1[indexPath.row]
        
        cell.name.text = achievement.name
        cell.art.image = achievement.image ?? UIImage()
        cell.additional.text = achievement.description
        
        return cell
    }
}
