//
//  ProjectsTableViewController.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 10/14/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

class ProjectsTableViewController: UITableViewController {

    //MARK: Data structures
    
    struct Project {
        let date: Date
        let name: String
        let status: String
        
        init(projectUserInfo: StudentModel.ProjectUserInfo, allProjects: [StudentModel.ProjectUserInfo]) {
            var parentName = ""
            if  let parentId = projectUserInfo.project?.parent,
                let parentProject = allProjects.first(
                    where: { $0.project?.id == parentId }),
                let Name = parentProject.project!.name {
                parentName = Name + " - "
            }
            
            self.name = parentName + (projectUserInfo.project?.name ?? "")
            
            if projectUserInfo.validated ?? false,
                let mark = projectUserInfo.finalMark {
                self.status = String(mark) + "%"
            }
            else if projectUserInfo.finalMark != nil {
                self.status = "Failed"
            }
            else {
                let str = (projectUserInfo.status ?? "").replacingOccurrences(of: "_", with: " ")
                self.status = str.prefix(1).uppercased() + str.lowercased().dropFirst()
            }
            
            if let date = projectUserInfo.date {
                self.date = DateFormatter().date(fromSwapiString: date)!
            } else {
                self.date = Date()
            }
            
        }
    }
    
    //MARK: Properties
    
    var projects: [(String, [Project])] = []

    //MARK: Methods
    
    func mapProjectsToCursus(student: StudentModel) -> [Int:[Project]] {
        var projectsMap: [Int:[Project]] = [:]
        for project in student.projectsUserInfo {
            guard project.status != "parent" else {
                continue
            }
            for cursusId in project.cursusIds {
                if projectsMap[cursusId] == nil {
                    projectsMap[cursusId] = []
                }
                projectsMap[cursusId]!.append(Project.init(projectUserInfo: project, allProjects: student.projectsUserInfo))
            }
        }
        return projectsMap
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard projects.isEmpty else {
            return
        }
        
        let currentStudent = GetCurrentStudentData()
        let projectsPerCursus = mapProjectsToCursus(student: currentStudent).sorted(by: { $0.key < $1.key })
        projects = projectsPerCursus.compactMap {
            let currentId = $0
            let cursusName = currentStudent.cursusUserInfo.first(where: { $0.cursusId == currentId })?.cursus?.name
            return (cursusName ?? "Other", $1)
        }

        projects = projects.map{ ( $0.0, $0.1.sorted(by: { $0.date.compare($1.date) == .orderedDescending }) ) }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.title = "Projects"
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return projects.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects[section].1.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return projects[section].0.capitalized
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectTableViewCell", for: indexPath) as? ProjectTableViewCell  else {
            fatalError("The dequeued cell is not an instance of ProjectTableViewCell.")
        }
        
        let currentProject = projects[indexPath.section].1[indexPath.row]
        
        cell.nameLabel.text = currentProject.name
        cell.statusLabel.text = currentProject.status
        return cell
    }
}
