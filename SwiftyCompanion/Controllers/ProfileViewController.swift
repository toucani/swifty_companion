//
//  StudentInfoVC.swift
//  
//
//  Created by Dmytro Kovalchuk on 10/8/18.
//

import UIKit
import MessageUI
import UIColor_Hex_Swift

class ProfileViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    //MARK: Properties
    
    @IBOutlet weak var profileInfoView: ProfileInfoView!
    @IBOutlet weak var levelWalCorView: LevelWallCorrView!

    //skills view
    
    //MARK: Methods
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func getCountry(student: StudentModel!) -> String {
        //Look for primary campus id
        var primaryCampusId = -1
        for campusUsers in student.campusUsers {
            if campusUsers.isPrimary ?? false {
                primaryCampusId = campusUsers.campusId ?? -1
                break;
            }
        }
        
        //Look for the capmus data itself
        var primaryCampus = student.campus[0]
        for campus in student.campus {
            if campus.id == primaryCampusId {
                primaryCampus = campus
                break;
            }
        }
        
        //City + country
        if let city = primaryCampus.city {
            return city + (primaryCampus.country != nil ? ("/" + (primaryCampus.country)!) : "")
        }
        return ""
    }
    
    func getLocation(student: StudentModel!, staff: Bool) -> String? {
        return staff ? "Staff" : student.location
    }
    
    func setUpProfileInfo(student: StudentModel!, staff: Bool) {
        guard
            let url = URL(string: student.imageURL ?? ""),
            let image = try? UIImage(data: Data(contentsOf: url)) else {
                return
        }
        
        profileInfoView.set(photo: image)
        profileInfoView.set(login: student.login, grade: !staff && !student.cursusUserInfo.isEmpty ? student.cursusUserInfo[0].grade : nil)
        profileInfoView.set(name: student.displayName ?? "")
        profileInfoView.set(country: getCountry(student: student))
        profileInfoView.set(location: getLocation(student: student, staff: staff))
        
        //Calling the person
        if let phone = GetCurrentStudentData().phone,
            !phone.isEmpty && UIApplication.shared.canOpenURL(URL(string: "tel://")!) {
            profileInfoView.set(phoneButtonCallback: {
                UIApplication.shared.open(URL(string: "tel://" + phone)!)})
        } else {
            profileInfoView.set(phoneButtonCallback: nil)
        }
        
        //Composing email
        if let adress = student.email,
            !adress.isEmpty && MFMailComposeViewController.canSendMail() {
            profileInfoView.set(emailButtonCallback: {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients([adress])
                self.present(mail, animated: true)
            })
        } else {
            profileInfoView.set(emailButtonCallback: nil)
        }
    }
    
    func setUpLevelCorrectionsWallets(student: StudentModel!, staff: Bool) {
        levelWalCorView.isHidden = staff

        guard !staff else {
            return
        }
        
        levelWalCorView.set(corrections: uint(student.correctionPoint ?? 0),
                            level: Float(student.cursusUserInfo[0].level ?? 0),
                            wallets: uint(student.wallet ?? 0))
    }
    
    func setUpSkills(student: StudentModel!, staff: Bool) {
//        skills.isHidden = staff
        
        guard !staff else {
            return
        }
    }
    
    override func viewDidLoad() {
        let studentData = GetCurrentStudentData()
        let isStaff = studentData.staff ?? false
        
        setUpProfileInfo(student: studentData, staff: isStaff)
        setUpLevelCorrectionsWallets(student: studentData, staff: isStaff)
        setUpSkills(student: studentData, staff: isStaff)
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.title = "Profile"
    }
}
