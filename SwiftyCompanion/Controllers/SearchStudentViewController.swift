//
//  LoginViewController.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 9/22/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit
import SVGKit
import Alamofire

class SearchStudentViewController: UIViewController {

    //MARK: Properties
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var xloginField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: Actions
    
    @IBAction func changedValue(_ sender: Any) {
        searchButton.isEnabled = isLoginValid(login: xloginField.text)
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        xloginField.endEditing(true)
        doTheSearch()
    }
    
    //MARK: Methods
    
    func cleanErrorMessage() {
        errorLabel.text = ""
    }
    
    func showErrorMessage(message: String) {
        showErrorMessage(title: nil, message: message)
    }
    
    func showErrorMessage(title: String?, message: String) {
        var fullTitle = ""
        if let title = title {
            fullTitle += title + ": "
        }
        errorLabel.text = fullTitle + message;
    }
    
    func enterWaitingMode(wait: Bool, silent: Bool = false) {
        if (wait) {
            self.xloginField.isEnabled = false
            self.searchButton.isHidden = !silent
            self.activityIndicator.startAnimating()
        } else {
            let token = GetToken()
            self.xloginField.isEnabled = true
            self.xloginField.isHidden = token.isEmpty
            self.searchButton.isHidden = token.isEmpty
            self.activityIndicator.stopAnimating()
        }
        self.xloginField.isUserInteractionEnabled = !wait
        self.searchButton.isUserInteractionEnabled = !wait
        cleanErrorMessage()
    }
    
    func isLoginValid(login: String?, showError: Bool = false) -> Bool {
        let result = login != nil && !(login!.isEmpty) && !(login!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
        if showError && !result {
            showErrorMessage(title: "O rly?", message: "Won't look for an empty string...")
        }
        return result
    }

    func getToken() {
        enterWaitingMode(wait: true)
        RequestToken(
            onSuccess: { dummy in self.enterWaitingMode(wait: false) },
            onFail: { title, description in
                self.enterWaitingMode(wait: false)
                self.showErrorMessage(title: title ?? "No response from the server",
                                      message: description ?? "Are you connected to the internet?")
        })
    }
    
    func doTheSearch() {
        guard isLoginValid(login: xloginField.text, showError: true) else {
            return
        }
        xloginField.endEditing(true)
        self.enterWaitingMode(wait: true)
        DownloadStudentData(login: xloginField.text!,
                            onSuccess: { student in
                                SetCurrentStudent(login: student)
                                self.performSegue(withIdentifier: "showStudentProfile", sender: nil)
                                self.enterWaitingMode(wait: false) },
                            onFail: { title, description in
                                self.enterWaitingMode(wait: false)
                                self.showErrorMessage(title: title ?? "No such student",
                                                      message: description ?? "Double check the login.")
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        xloginField.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    override func viewDidLoad() {
        cleanErrorMessage()
        searchButton.isEnabled = false
        getToken()
        super.viewDidLoad()
    }
}
