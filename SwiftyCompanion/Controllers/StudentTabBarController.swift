//
//  StudentTB.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 10/13/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

class StudentTabBarController: UITabBarController {    
    override func viewDidLoad() {
        let student = GetCurrentStudentData()
        //Disabling Projects for staff
        self.tabBar.items![2].isEnabled = !(student.staff ?? false)
//        self.title = student.login!
    }
}
