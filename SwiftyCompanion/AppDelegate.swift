//
//  AppDelegate.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 9/22/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var accessToken = ""
    var currentStudent = ""
    var currentCoalition: CoalitionInfo?
    var studentsDataCache: [String: StudentModel] = [:]
    var imageCache: [String: UIImage] = [:]
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        let copy = studentsDataCache[currentStudent]
        studentsDataCache.removeAll()
        studentsDataCache[currentStudent] = copy
    }
}
