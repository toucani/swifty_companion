//
//  DataModel.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 10/7/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import Foundation

struct StudentModel: Decodable {
    
    let id: Int
    let displayName: String?
    let login: String
    let staff: Bool?
    let email: String?
    let phone: String?
    let imageURL: String?
    let location: String?
    let wallet: Int?
    let correctionPoint: Int?
    let campus: [Campus]
    let campusUsers: [CampusUser]
    let achievements: [Achievement]
    let cursusUserInfo: [CursusUserInfo]
    let projectsUserInfo: [ProjectUserInfo]
}

extension StudentModel {
    
    enum CodingKeys: String, CodingKey {
        
        case id
        case displayName = "displayname"
        case login
        case staff = "staff?"
        case email
        case phone
        case imageURL = "image_url"
        case location
        case wallet
        case campus
        case campusUsers = "campus_users"
        case correctionPoint = "correction_point"
        case achievements
        case cursusUserInfo = "cursus_users"
        case projectsUserInfo = "projects_users"
    }
    
    struct Campus: Decodable {
        
        let id: Int?
        let country: String?
        let city: String?
    }

    struct CampusUser: Decodable {
        
        let campusId: Int?
        let isPrimary: Bool?
    }
    
    struct Achievement: Decodable {
        
        let id: Int?
        let name: String?
        let kind: String?
        let description: String?
        let visible: Bool?
        let image: String?
    }
    
    struct CursusUserInfo: Decodable {
        
        let grade: String?
        let level: Double?
        let skills: [Skills]
        let cursusId: Int?
        let cursus: Cursus?
    }
    
    struct ProjectUserInfo: Decodable {
        
        let project: Project?
        let status: String?
        let validated: Bool?
        let cursusIds: [Int]
        let finalMark: Int?
        let date: String?
    }
}

extension StudentModel.Campus {
    
    enum CodingKeys: String, CodingKey {
        
        case id
        case country
        case city
    }
}

extension StudentModel.CampusUser {
    
    enum CodingKeys: String, CodingKey {
        
        case campusId = "campus_id"
        case isPrimary = "is_primary"
    }
}

extension StudentModel.Achievement {
    
    enum CodingKeys: String, CodingKey {
        
        case id
        case name
        case kind
        case description
        case visible
        case image
    }
}

extension StudentModel.CursusUserInfo {
    
    enum CodingKeys: String, CodingKey {
        
        case skills
        case grade
        case level
        case cursusId = "cursus_id"
        case cursus
    }
    
    struct Skills: Decodable {
        
        let id: Int?
        let name: String?
        let level: Double?
    }
    
    struct Cursus: Decodable {
        
        let id: Int?
        let date: String?
        let name: String?
    }
}

extension StudentModel.ProjectUserInfo {
    
    enum CodingKeys: String, CodingKey {
        
        case project
        case status
        case validated = "validated?"
        case cursusIds = "cursus_ids"
        case finalMark = "final_mark"
        case date = "marked_at"
    }
    
    struct Project: Decodable {
        
        let id: Int?
        let name: String?
        let parent: Int?
    }
}

extension StudentModel.CursusUserInfo.Skills {
    
    enum CodingKeys: String, CodingKey {
        
        case id
        case name
        case level
    }
}

extension StudentModel.CursusUserInfo.Cursus {
    
    enum CodingKeys: String, CodingKey {
        
        case id
        case date = "created_at"
        case name
    }
}

extension StudentModel.ProjectUserInfo.Project {
    
    enum CodingKeys: String, CodingKey {
        
        case id
        case name
        case parent = "parent_id"
    }
}

typealias CoalitionsInfo = [CoalitionInfo]

struct CoalitionInfo: Decodable {
    let name: String
    let image: String
    let color: String
    let score: Int
}

extension CoalitionInfo {
    
    enum CodingKeys: String, CodingKey {
        
        case name
        case image = "image_url"
        case color = "color"
        case score
    }
}
