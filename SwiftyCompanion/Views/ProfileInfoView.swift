//
//  ProfileInfoView.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 11/24/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

class ProfileInfoView: NibLoadable {

    //MARK: Properties
    
    private var emailCallback: (() -> Void)? = nil
    private var phoneCallback: (() -> Void)? = nil
    
    @IBOutlet weak var photoView: UIImageView!
    
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    //MARK: Actions
    
    @IBAction func callButtonPressed(_ sender: UIButton) {
        if let closure = phoneCallback {
            closure()
        }
    }
    
    @IBAction func emailButtonPressed(_ sender: UIButton) {
        if let closure = emailCallback {
            closure()
        }
    }
    
    //MARK: Methods
    
    func set(photo: UIImage?) {
        photoView.image = photo
        photoView.layer.cornerRadius = photoView.frame.width / 2
        photoView.clipsToBounds = true
    }
    
    func set(login: String, grade: String?) {
        var fullString = "#" + login
        if let grade = grade {
            fullString += " | " + grade.lowercased()
        }
        loginLabel.text = fullString
    }
    
    func set(name: String) {
        nameLabel.text = name
    }
    
    func set(country: String) {
        countryLabel.text = country
    }
    
    func set(location: String?) {
        locationLabel.text = location ?? "unavailable"
        locationLabel.textColor = (location != nil) ? UIColor.white : UIColor(named: "Blue text")
    }
    
    func set(emailButtonCallback: (() -> Void)?) {
        emailCallback = emailButtonCallback
        emailButton.isEnabled = emailButtonCallback != nil
    }
    
    func set(phoneButtonCallback: (() -> Void)?) {
        phoneCallback = phoneButtonCallback
        callButton.isEnabled = phoneButtonCallback != nil
    }
}
