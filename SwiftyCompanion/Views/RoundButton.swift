//
//  RoundButton.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 11/27/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

@IBDesignable
class RoundButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override func prepareForInterfaceBuilder() {
        commonInit()
    }
    
    func commonInit() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
}
