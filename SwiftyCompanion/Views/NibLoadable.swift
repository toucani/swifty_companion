//
//  NibLoadable.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 11/24/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

@IBDesignable
class NibLoadable: UIView {
    var view: UIView!
    
    //When creating the view in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    //When creating the view in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        view.frame = bounds
        view.autoresizingMask = [
            UIView.AutoresizingMask.flexibleWidth,
            UIView.AutoresizingMask.flexibleHeight
        ]
        addSubview(view)
        self.view = view
    }

}
