//
//  ProjectTableViewCell.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 10/14/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {

    //MARK: Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    //MARK: Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
