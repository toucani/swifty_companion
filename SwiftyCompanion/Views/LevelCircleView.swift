//
//  CirclePieView.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 10/20/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

@IBDesignable
class LevelCircleView: UIView {
    
    //MARK: Properties
    
    @IBInspectable var innerRadius: UInt = 5
    @IBInspectable var levelColor : UIColor = .black
    @IBInspectable var emptyColor : UIColor = .white
    
    @IBInspectable var currentLevel : Float = 14.24
    private let maximumLevel : Float = 21.0
    
    //MARK: Methods
    
    //When creating the view in code
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //When creating the view in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func set(level: Float) {
        currentLevel = level
    }
    
    override func draw(_ rect: CGRect) {
        //Base circle
        emptyColor.setFill()
        let outerPath = UIBezierPath(ovalIn: rect)
        outerPath.fill()
        
        //self.frame isn't defined yet, so we can't use self.center
        let viewCenter = CGPoint(x: rect.width / 2, y: rect.height / 2);
        let baseCircleRadius = rect.width / 2
        let startOffset = -90
        //Since we're filling counter clokwise, gotta take the difference 360 - amount.
        let endAngle = 360 - (360 * currentLevel / maximumLevel) + Float(startOffset);
        
        levelColor.setFill()
            
        let midPath = UIBezierPath()
        midPath.move(to: viewCenter)
        midPath.addArc(withCenter: viewCenter, radius: CGFloat(baseCircleRadius),
                       startAngle: Float(startOffset).degreesToRadians,
                       endAngle: Float(endAngle).degreesToRadians, clockwise: false)
        midPath.close()
        midPath.fill()
        
        //Center circle
        let centerCircleRadius = baseCircleRadius * 1 / CGFloat(innerRadius)
        UIColor.white.setFill()
        let centerPath = UIBezierPath(ovalIn:
            rect.insetBy(dx: centerCircleRadius, dy: centerCircleRadius))
        centerPath.fill()
    }
}

extension Float {
    var degreesToRadians : CGFloat {
        return CGFloat(self) * CGFloat(Double.pi) / 180.0
    }
}
