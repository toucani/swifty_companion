//
//  CoalitionInfoView.swift
//  
//
//  Created by Dmytro Kovalchuk on 12/2/18.
//

import UIKit

@IBDesignable
class CoalitionInfoView: NibLoadable {

    //MARK: Properties

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var gobletImage: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    //MARK: Methods
    
    func set(color: UIColor) {
        backgroundImage.tintColor = color
        gobletImage.tintColor = color
        nameLabel.textColor = color
        scoreLabel.textColor = color
    }
    
    func set(logo: UIImage) {
        logoImage.image = logo
    }
    
    func set(name: String) {
        nameLabel.text = name
    }
    
    func set(score: Int) {
        scoreLabel.text = String(score)
    }
}
