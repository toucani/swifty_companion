//
//  LevelWallCorrView.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 11/24/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

@IBDesignable
class LevelWallCorrView: NibLoadable {
    
    //MARK: Properties
    
    @IBOutlet private weak var levelLabel: UILabel!
    @IBOutlet private weak var walletsLabel: UILabel!
    @IBOutlet private weak var correctionsLabel: UILabel!
    
    @IBOutlet private weak var levelCircle: LevelCircleView!
    
    //MARK: Methods
    
    func set(level: Float) {
        levelLabel.text = String(level)
        levelCircle.set(level: level)
    }
    
    func set(wallets: uint) {
        walletsLabel.text = String(wallets) + "₳"
    }

    func set(corrections: uint) {
        correctionsLabel.text = String(corrections)
    }
    
    func set(corrections: uint, level: Float, wallets: uint) {
        set(level: level)
        set(corrections: corrections)
        set(wallets: wallets)
    }
}
