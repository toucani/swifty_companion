//
//  AchievementTableViewCell.swift
//  SwiftyCompanion
//
//  Created by Dmytro Kovalchuk on 10/14/18.
//  Copyright © 2018 dkovalch. All rights reserved.
//

import UIKit

class AchievementTableViewCell: UITableViewCell {

    //MARK: Properties
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var art: UIImageView!
    @IBOutlet weak var additional: UILabel!
    
    //MARK: Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
