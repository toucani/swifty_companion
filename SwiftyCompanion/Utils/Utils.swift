//
//  Print.swift
//  
//
//  Created by Dmytro Kovalchuk on 10/7/18.
//

import UIKit
import SVGKit
import Alamofire
import Foundation

func PrintDebug(_ object: Any) {
    #if DEBUG
        Swift.print(object)
    #endif
}

func SetCurrentStudent(login: String) {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    delegate.currentStudent = login
}

func GetStudentData(login: String) -> StudentModel? {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    return delegate.studentsDataCache[login]
}

func SetStudentData(login: String, data: StudentModel) {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    delegate.studentsDataCache[login] = data
}

func GetCurrentStudentData() -> StudentModel {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    return GetStudentData(login: delegate.currentStudent)!
}

func GetCurrentCoalitionInfo() -> CoalitionInfo? {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    return delegate.currentCoalition
}

func GetCachedImage(key: String) -> UIImage? {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    return delegate.imageCache[key]
}

func GetToken() -> String {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    return delegate.accessToken
}

func RequestToken(onSuccess: ((String) -> Void)?, onFail: ((String?, String?) -> Void)?) {
    let params = [
        "grant_type" : "client_credentials",
        "client_id": "571699045ef85a1635c94e5b4e7a2a041b04a76c01f8b24520a603539137ae48",
        "client_secret": "8e15f5a0782a3f1ecb8ea3ed04e69d1310f43679c560cf0b7ba5188ca5d9ebfd"]
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    delegate.accessToken = ""
    
    Alamofire.request("https://api.intra.42.fr/oauth/token",
                      method : .post, parameters : params).responseJSON {
        response in
        
        if response.result.isSuccess {
            let jsonObject = response.result.value as! [String : Any]
            
            //Setting up the timer to get a new token when this one expires
            if jsonObject["expires_in"] != nil {
                PrintDebug("Setting up a timer to get a new token in " +
                    (jsonObject["expires_in"] as! NSNumber).stringValue + " seconds.")
                DispatchQueue.main.asyncAfter(
                    deadline: .now() + .seconds(jsonObject["expires_in"] as! Int),
                    execute: { RequestToken(onSuccess: nil, onFail: nil) })
            }
            
            //Looking for the token itself
            if jsonObject["access_token"] != nil {
                delegate.accessToken = jsonObject["access_token"] as! String
                PrintDebug("Got new authorization token!")
                onSuccess?(delegate.accessToken)
            } else {
                onFail?(jsonObject["error"] as? String, jsonObject["error_description"] as? String)
            }
        } else {
            onFail?(nil, nil)
        }
    }
}

func DownloadSVGImage(link: String, key: String? = nil) {
    //Has the image already been loaded?
    guard GetCachedImage(key: link) == nil else {
        return
    }

    //Lets get it.
    Alamofire.request(link, method: .get).responseString {
        response in
        guard response.result.isSuccess else {
            return
        }
        let value = response.value
        let data = value?.data(using: .utf8)
        if value != "{}" && data != nil {
            let anSVGImage: SVGKImage = SVGKImage(data: data!)
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.imageCache[key ?? link] = anSVGImage.uiImage
        }
    }
}

func DownloadAchievementImages(login: String) {
    guard let achievements = GetStudentData(login: login)?.achievements else {
        return
    }
    
    for each in achievements {
        guard each.visible ?? true && each.image != nil else {
            continue
        }
        DownloadSVGImage(link: "https://api.intra.42.fr" + each.image!, key: each.image!)
    }
}

func DownloadStudentData(login: String, onSuccess: ((String) -> Void)?, onFail: ((String?, String?) -> Void)?) {
    
    //Do we have the student's data in cache?
    if GetStudentData(login: login) != nil {
        onSuccess?(login)
        return
    }
    
    //No. Gotta get the data from the server
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    
    let urlString = "https://api.intra.42.fr/v2/users/" + login
    Alamofire.request(urlString, method: .get, parameters: ["access_token": GetToken()]).responseString {
        response in
        PrintDebug(response)
        
        if response.result.isSuccess {
            let value = response.value
            let data = value?.data(using: .utf8)
            if value != "{}" && data != nil {
                do {
                    SetStudentData(login: login, data: try JSONDecoder().decode(StudentModel.self, from: data!))
                    DownloadAchievementImages(login: login)
                    DownloadCoalitionInfo(login: login, onSuccess: onSuccess, onFail: onFail)
                } catch {
                    onFail?("Cannot decode", "Failed to decode the message from the server.")
                }
            } else {
                onFail?(nil, nil)
            }
        } else {
            onFail?(nil, nil)
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

func DownloadCoalitionInfo(login: String, onSuccess: ((String) -> Void)?, onFail: ((String?, String?) -> Void)?) {
    let urlString = "https://api.intra.42.fr/v2/users/" + login + "/coalitions"

    Alamofire.request(urlString, method: .get, parameters: ["access_token": GetToken()]).responseString {
        response in
        PrintDebug(response)
        
        if response.result.isSuccess {
            let value = response.value
            let data = value?.data(using: .utf8)
            if value != "{}" && data != nil {
                let delegate = UIApplication.shared.delegate as! AppDelegate
                delegate.currentCoalition = (try? JSONDecoder().decode(CoalitionsInfo.self, from: data!))?[0]
                if let imageUrl = delegate.currentCoalition?.image {
                    let data = try? Data(contentsOf: URL(string: imageUrl)!)
                    delegate.imageCache[imageUrl] = SVGKImage(data: data)?.uiImage
                }
                onSuccess?(login)
            }
        } else {
            onFail?(nil, nil)
        }
    }
}

extension DateFormatter {
    func date(fromSwapiString dateString: String) -> Date? {
        // SWAPI dates look like: "2014-12-10T16:44:31.486000Z"
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        self.timeZone = TimeZone(abbreviation: "UTC")
        self.locale = Locale(identifier: "en_US_POSIX")
        return self.date(from: dateString)
    }
}
