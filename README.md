# Swifty companion

This is a simple IOs app used to serch a 42 student and view their main stats. This project is unfinished(that means I never pushed it to vogosphere also)! The code here was written to be easily reusable and understandable. All the icons and images were made specifically for the app(except coalitions logo and backgrounds), they can be found in design.zip.

Unfortunately, this won't work out of the box anymore due to expired token.

## Screenshots
![pic](Screenshots/0-Main_screen.png)
![pic](Screenshots/1-Student_info1.png) ![pic](Screenshots/2-Student_info2.png)
![pic](Screenshots/3-Projects.png) ![pic](Screenshots/4-Achievements.png)

## Installing

Clone this repository:
```
git clone
```
, move into the folder
```
cd swifty_companion/SwiftyCompanion
```
install pods([install CocoaPods first](https://guides.cocoapods.org/using/getting-started.html))
```
pod install
```
and
```
open SwiftyCompanion.xcworkspace
```

## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=swifty_companion from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to my ~~girlfriend~~ wife for the design.
* to UNIT Factory, for inspiration to do my best.
* to all UNIT Factory students, who shared their knowledge with me and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
